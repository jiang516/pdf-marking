export default {
  data() {
    return {}
  },
  computed: {
    rootVm() {
      if (this.parent.root) return this.parent
      else return this.parent.parent
    },
    parent() {
      return this.$parent.$parent
      // return this.$parent.$parent.$parent
      // return this.$parent
    },
    isDebugger() {
      return this.parent.isDebugger
    },
    scale() {
      return this.parent.scale
    },
    scrollTop() {
      return this.parent.scrollTop
    },
    scrollLeft() {
      return this.parent.scrollLeft
    },
    imageSize() {
      return this.parent.imageSize
    },
    stageSize() {
      return this.parent.stageSize
    },
    currPageIndex() {
      return this.parent.currPageIndex
    },
    pageCount() {
      return this.parent.pageCount
    },
    scaleType() {
      return this.parent.scaleType
    },
    rotation() {
      return this.parent.rotation
    },
    markList() {
      return this.parent.markList
    },
    progress() {
      return this.parent.progress
    },
    isScrawl() {
      return this.parent.isScrawl
    },
    scrawlList() {
      return this.parent.scrawlList
    },
    scrawlShow() {
      return this.parent.scrawlShow
    },
    markShow() {
      return this.parent.markShow
    },
    watermark() {
      return this.parent.watermark
    },
    pointerType() {
      return this.parent.pointerType
    },
    logger() {
      return this.parent.logger
    },
    isClip() {
      return this.parent.isClip
    },
    clipData() {
      return this.parent.clipData
    },
    seachResultIndex() {
      return this.parent.seachResultIndex
    },
    searchResultList() {
      return this.parent.searchResultList
    },
    alwaysShowMarkTypes() {
      return this.parent.alwaysShowMarkTypes
    },
    scrawlType() {
      return this.parent.scrawlType
    },
    shadowConfig() {
      return this.parent.shadowConfig
    },

    cursor: {
      get() {
        return this.parent.cursor
      },
      set(val) {
        this.parent.cursor = val
      }
    },

    sendEvent() {
      return this.parent.sendEvent
    },
    getStage() {
      return this.parent.getStage
    },
    getWrodsAToB() {
      return this.parent.getWrodsAToB
    },
    getWordByIndexPos() {
      return this.parent.getWordByIndexPos
    },
    removeScrawl() {
      return this.parent.removeScrawl
    },
  },
  methods: {}
}
