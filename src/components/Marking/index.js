// import $Vue from 'vue/dist/vue.esm'
import Component from './src/Component2.vue'
// import Component from './src/Component1.vue'

export {SCRAWLTYPE, POINTERTYPE} from './src/Component2'
// import Component from './src/Component.vue'
import {version} from '../../../package'

// import VueKonva from 'vue-konva'

// $Vue.use(VueKonva)

const install = Vue => {
  Vue.component('pdf-marking', Component)
  Vue.component('marking', Component)
}
Component.install = install
Component.version = version

export default Component
