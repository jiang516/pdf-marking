// export default class MyResizeObserver {
//   constructor (el, fn, immediate) {
//     if (immediate) {
//       fn({
//         width: el.offsetWidth,
//         height: el.offsetHeight
//       })
//     }
//     let ro = new ResizeObserver(entries => {
//       fn({
//         width: entries[0].contentRect.width,
//         height: entries[0].contentRect.height
//       })
//     })
//     ro.observe(el)
//   }
// }
export default class MyResizeObserver {
  constructor (el, fn, immediate) {
    if (immediate) {
      fn({
        width: el.clientWidth,
        height: el.clientHeight
      })
    }
    if (window.ResizeObserver) {
      let ro = new ResizeObserver(entries => {
        fn({
          width: entries[0].contentRect.width,
          height: entries[0].contentRect.height
        })
      })
      ro.observe(el)
    } else {
      let iframe = document.createElement("iframe")
      iframe.width = '100%'
      iframe.height = '100%'
      iframe.style.visibility = 'hidden'
      iframe.style.position = 'absolute'
      iframe.style.top = '0'
      iframe.style.left = '0'
      el.appendChild(iframe)
      iframe.contentWindow.addEventListener('resize', () => {
        fn({
          width: el.clientWidth,
          height: el.clientHeight
        })
      })
    }
  }
}
