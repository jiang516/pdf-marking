const allEvent = []

export function addEventListener (eventName, listener, el = window) {
  let newListener = e => {
    if (e.type === 'touchmove') e = e.targetTouches[0]
    listener(e)
  }
  allEvent.push({eventName, listener, newListener})
  if (eventName === 'mousemove') {
    el.addEventListener('mousemove', newListener)
    el.addEventListener('touchmove', newListener)
  } else if (eventName === 'mouseup') {
    el.addEventListener('mouseup', newListener)
    el.addEventListener('touchend', newListener)
  }
}

export function removeEventListener (eventName, listener, el = window) {
  let idx = allEvent.findIndex(item => item.eventName === eventName && item.listener === listener)
  let _event = allEvent[idx]
  let newListener = listener
  if (_event) {
    newListener = _event.newListener
    allEvent.splice(idx, 1)
  }
  if (eventName === 'mousemove') {
    el.removeEventListener('mousemove', newListener)
    el.removeEventListener('touchmove', newListener)
  } else if (eventName === 'mouseup') {
    el.removeEventListener('mouseup', newListener)
    el.removeEventListener('touchend', newListener)
  }
}
