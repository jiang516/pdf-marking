export default function getPos (el, pEl) {
  let pos = {x: 0, y: 0}
  pos.height = el.offsetHeight
  pos.width = el.offsetWidth

  let $el = el
  do {
    if (el === pEl) break
    pos.x += el.offsetLeft
    pos.y += el.offsetTop
  } while ((el = el.offsetParent))

  el = $el
  do {
    if (el === pEl) break
    pos.x -= el.scrollLeft
    pos.y -= el.scrollTop
  } while ((el = el.parentElement))
  return pos
}

export function getPostion (el, pEl) {
  let pos = {}
  pos.x = el.offsetLeft
  pos.y = el.offsetTop
  while ((el = el.offsetParent)) {
    if (el === pEl) break
    pos.x += el.offsetLeft
    pos.y += el.offsetTop
  }
  pos.height = pEl ? pEl.offsetHeight : window.innerHeight
  pos.width = pEl ? pEl.offsetWidth : window.innerWidth
  return pos
}
