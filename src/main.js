import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// window.addEventListener("resize", (e) => {
//   setTimeout(() => {
//     alert(window.outerWidth)
//   }, 1000)
// })

new Vue({
  render: h => h(App),
}).$mount('#app')
