// git commit -m "提交代码"
// git push origin master

const {cmdExec} = require('./cmd')
const pkg = require('../package');

(async () => {
  await cmdExec(`git commit -a -m "提交代码 ${pkg.version} ${new Date().toLocaleString()}"`)
  await cmdExec(`git push origin master`)
})()
