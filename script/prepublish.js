const fs = require('fs')
const path = require('path')

const {cmdExec} = require('./cmd')

let pkg = fs.readFileSync(path.resolve(__dirname, '../package.json')).toString()
let pkgJSON = JSON.parse(pkg);

(async () => {
  // await cmdExec(`npm unpublish ${pkgJSON.name}@${pkgJSON.version}`)

  if (/(-?\d+)\.(-?\d+)\.(-?\d+)/.test(pkgJSON.version)) {
    // pkgJSON.oldVersion = pkgJSON.version
    pkgJSON.version = `${RegExp.$1}.${RegExp.$2}.${Number(RegExp.$3) + 1}`
  }
  // fs.writeFileSync(path.resolve(__dirname, '../package.json'), JSON.stringify(pkgJSON, null, 2))
})()

